#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/sysinfo.h>

void get_systeminfo()
{
  struct sysinfo info;

  if (sysinfo(&info) == 0)
  {
	  printf("loadaverage1: %lu\n",info.loads[0]);
	  printf("loadaverage5: %lu\n",info.loads[1]);
	  printf("loadaverage15: %lu\n",info.loads[2]);
	  printf("bufferram: %lu\n",info.bufferram);
	  printf("sharedram: %lu\n",info.sharedram);
	  printf("freeram: %lu\n",info.freeram);
	  printf("freeswap: %lu\n",info.freeswap);
	  printf("procs: %lu\n",info.procs);
	  printf("freehigh: %lu\n",info.freehigh);
	  printf("mem_int: %d\n",info.mem_unit);
	  printf("uptime: %ld\n",info.uptime);
  }
}

void milliseconds_sleep(unsigned long mSec){
    struct timeval tv;
    tv.tv_sec=mSec/1000;
    tv.tv_usec=(mSec%1000)*1000;
    int err;
    do{
       err=select(0,NULL,NULL,NULL,&tv);
    }while(err<0 && errno==EINTR);
}

int main()
{
    get_systeminfo();

	return 0;
}


